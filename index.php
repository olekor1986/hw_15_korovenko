<?php
$search_words = ['php', 'html', 'интернет', 'Web'];
$search_strings = [
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
];

function search($wordsArray, $stringsArray)
{
    $result = '';
    foreach ($stringsArray as $strings_key => $string) {
        $words = '';
        foreach ($wordsArray as $word) {
            if (preg_match("/($word)/iu", $string)) {
                $words .= $word;
                $words .= ", ";
            }
        }
        $words = substr($words, 0, -2);
        $result .=  "В предложении №" . ($strings_key + 1) . " есть слова: " . $words . "." . "<br>";
    }
    return $result;
}

$resultString = search($search_words, $search_strings);
echo $resultString;
?>

